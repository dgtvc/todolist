import axios from 'axios'

export default class api {

    constructor() {
        this.API_URL = '/api/v1'
        this.API_PATH = '/lists'
    }


    async index() {
        const { data } = await axios.get(this.API_URL + this.API_PATH)
        return data
    }

    async get( id ) {
        let { data } = await axios.get(this.API_URL + this.API_PATH + '/' + id)
        return data
    }

    async store(save) {
        let { data } = await axios.post(this.API_URL + this.API_PATH, {
            ...save
        })
        return data
    }

    async update(id, save) {
        let { data } = await axios.post(this.API_URL + this.API_PATH, {
            id: id,
            ...save
        })
        return data
    }


    async delete(id) {
        let { data } = await axios.delete(this.API_URL + this.API_PATH + '/' + id)
        return data
    }

}