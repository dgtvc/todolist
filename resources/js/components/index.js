import Vue from 'vue'
import Card from './Card'
import Child from './Child'
import Button from './Button'
import Checkbox from './Checkbox'
import Toasted from 'vue-toasted'
import {HasError, AlertError, AlertSuccess} from 'vform'

// Components that are registered globaly.
[
    Card,
    Child,
    Button,
    Checkbox,
    HasError,
    AlertError,
    AlertSuccess
].forEach(Component => {
    Vue.component(Component.name, Component)
})

Vue.use(Toasted, {
    position: 'top-bottom',
    duration: 7000
})
