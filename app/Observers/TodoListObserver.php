<?php

namespace App\Observers;

use App\Models\TodoList;
use Illuminate\Support\Facades\Auth;

class TodoListObserver
{
    private $model;

    public function __construct()
    {
        $this->model = TodoList::query();
    }

    public function creating(TodoList $list) {
        $searching = true;
        while($searching) {
            $test_path = str_random(30);
            $test_path_count = $this->model->where('path','=', $test_path)->count();
            if(!$test_path_count) {
                $searching = false;
                $list->path = $test_path;
            }
        }
        $list->user_id = auth()->id();
    }
}
